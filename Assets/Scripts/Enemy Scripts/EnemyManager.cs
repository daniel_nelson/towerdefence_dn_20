﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

/*
 * Maily keeps track of the enemies - and also match settings
 * 
*/
public class EnemyManager : NetworkBehaviour {
    public static List<Vector3> path;
    public static EnemyManager singleton;
    public static int pathsUpdated = 0;
	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Enemy manager is running"); 
		} else {
			singleton = this;
        }
    }
    public static int GetPathIndexFromPos(Vector3 pos, float dev) {
        for(int i = 0; i < path.Count; i++) {
            if(Vector3.Distance(path[i], pos) < dev) 
                return i;
        }
        Debug.LogError("no mathing index");
        return 0;
    }
    public static Vector3 GroundY(Vector3 v) {
        return new Vector3(v.x, 0.1f, v.z);
    }
    public static bool CheckLineOfSight(Vector3 currentPos, List<Transform> edges, LayerMask mask) {
        foreach (Transform edge in edges) {
            Vector3 edgePos = GroundY(edge.position);
            float dis = Vector3.Distance(currentPos, edgePos);
            Vector3 dir = GroundY(currentPos) - edgePos;
            dir.Normalize();
            RaycastHit hit;
            if (Physics.Raycast(edgePos, dir, out hit, dis, mask)) {
                if (hit.transform.tag == "Map") {
                    return false;
                }
            }
        }
        return true;
    }
    public static Vector3? GetClosestPathNodePos(Vector3 currentPos, List<Transform> edges, LayerMask mask) {//TODO make sure this doesnt take up too much processing power (if it does maybe try a different find function)
        if (path == new List<Vector3>() || path == null) return new Vector3();
        Vector3? closest = null;
        float closestDis = 0;
        bool first = true;
        foreach (Vector3 pn in path) {
            //TODO add fallback
            //closest = pn;
           
            float dis = Vector3.Distance(currentPos, pn);
            if (CheckLineOfSight(pn, edges, mask)) {
                if (first) {
                    closest = pn;
                    closestDis = dis;
                    first = false;
                    continue;
                }
                if (dis < closestDis) {
                    closest = pn;
                    closestDis = dis;
                }
            }
        }
        return closest;
    }
    public static Vector3 GetClosestPathNodePos(Vector3 currentPos) {//TODO make sure this doesnt take up too much processing power (if it does maybe try a different find function)
        if (path == new List<Vector3>() || path == null) return new Vector3();
        Vector3 closest = Vector3.zero;
        float closestDis = 0;
        bool first = true;
        foreach (Vector3 pn in path) {
            float dis = Vector3.Distance(currentPos, pn);
            if (first) {
                closest = pn;
                closestDis = dis;
                first = false;
                continue;
            }
            if(dis < closestDis){
                closest = pn;
                closestDis = dis;
            }
        }
        return closest;
    }
    void Update() {
        pathsUpdated = 0;
    }
	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the enemy code
	/// </summary>
	#region enemy tracking 
	private const string ENEMY_ID_PREFIX = "Enemy "; 

	public static Dictionary<string, Enemy> enemies = new Dictionary<string, Enemy>();
    public static Dictionary<string, Enemy> spawnableEnemies = new Dictionary<string, Enemy>();
    public List<Enemy> spawnableEnemyList;
    public Pathfinder pathfinder;
    Vector3 playerBase;
    Vector3 spawnPos;
    public override void OnStartClient() {
        base.OnStartClient();
        if (!isServer) return;

        spawnableEnemies = new Dictionary<string, Enemy>();
        for (int i = 0; i < spawnableEnemyList.Count; i++) {
            string type = spawnableEnemyList[i].type;
            Enemy enemy = spawnableEnemyList[i];
            spawnableEnemies.Add(spawnableEnemyList[i].type, spawnableEnemyList[i]);
        }
        pathfinder = new Pathfinder();
        playerBase = GameObject.Find("EndZone").transform.position;
        spawnPos = GameObject.FindGameObjectWithTag("SpawnPos").transform.position;
        path = pathfinder.FindVector3Path(spawnPos, playerBase, true);
        if (path.Count == 0) {
            Debug.LogError("Nopath");
        }
    }
    public static bool OnPath(Vector3 pos, float rad) {
        pos = GroundY(pos);
        foreach(Vector3 v3 in path) {
            if (Vector3.Distance(v3, pos) < rad)
                return true;
        }
        return false;
    }
    public bool UpdatePath() {
        List<Vector3> newPath = pathfinder.FindVector3Path(spawnPos, playerBase, true);
        if (newPath.Count == 0) {
            Debug.Log("Nopath");
            return false;
        }
        path = newPath;
        return true;
    }
    //you can get a enemy from this getter
    public static Enemy GetEnemy(string _enemyID){
        if(enemies.ContainsKey(_enemyID))
		    return enemies[_enemyID];
        print("Couldn't find enemy with ID: " + _enemyID);
        return default(Enemy);
	}

	//Find the closest enemy to position
	public static Enemy GetClosestEnemy(Vector3 position){
		Enemy currentClosest = default(Enemy);
		float currentClosestDistance = 0f;
		foreach (Enemy enemy in enemies.Values) {
			float distance = Vector3.Distance (enemy.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = enemy;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Enemy)) {
			return currentClosest;
		}
		Debug.LogError ("No enemy found. Returned default enemy likely breaking script using this method");
		return default(Enemy);
	}
    #endregion


    Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range) {
        Vector3 spawnPos;
        spawnPos.x = centreSpawnPos.x + (Random.Range(-range, range));
        spawnPos.z = centreSpawnPos.z + (Random.Range(-range, range));
        spawnPos.y = centreSpawnPos.y;
        return spawnPos;
    }
    

    #region Enemy Functions
    //[Command]
    //public void CmdSpawnEnemy(string _type, Vector3 _pos, float _radius) {
    //    Enemy enemy = EnemyManager.spawnableEnemies[_type];

    //    Vector3 spawnPos = UpdateSpawnPos(_pos, _radius);
    //    GameObject obj = enemy.gameObject;
    //    obj = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);

    //    NetworkServer.Spawn(obj);
    //    string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
    //    obj.name = _type + " " + ID;
    //    enemy = obj.GetComponent<Enemy>();
    //    EnemyManager.enemies.Add(obj.name, enemy);
    //}
    public void SpawnEnemy(string _type, Vector3 _pos, float _radius) {
        if (isServer) {
            Enemy enemy = EnemyManager.spawnableEnemies[_type];

            Vector3 spawnPos = UpdateSpawnPos(_pos, _radius);
            GameObject obj = enemy.gameObject;
            obj = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);

            NetworkServer.Spawn(obj);

            obj.transform.position = spawnPos;
            string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
            obj.name = _type + " " + ID;
            enemy = obj.GetComponent<Enemy>();
            EnemyManager.enemies.Add(obj.name, enemy);
        }
    }
    [Command]
    public void CmdDestroyEnemy(Vector3 _pos) {
        Enemy enemy = default(Enemy);
        string ID = "";
        foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
            if (kp.Value.transform.position == _pos) {
                enemy = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (enemy != default(Enemy)) {
            EnemyManager.enemies.Remove(ID);
            NetworkServer.Destroy(enemy.gameObject);
        }
    }
    #endregion
}
