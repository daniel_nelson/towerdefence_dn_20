﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {
    [SerializeField]
    private List<Transform> edges;
    public const string PLACEHOLDER = "Placeholder";
	public const string FLYING = "Flying";
	public const string FASTFLYING = "FastFlying";
    [SerializeField]
    public string type;
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float weaponDamage, weaponRange, offsetToUpdatePath, moveSpeed, rotSpeed, viewDistance; 
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected int enemyId;
    bool canDeviate = true;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;

	protected Pathfinder pathfinder;
	protected Transform playerBase;
    protected Vector3 targetPos;

    public GameObject explosion;
	public AudioSource hummingSource;
	public AudioClip hummingSound;
	public AudioClip hummingDamageSound;
	public AudioSource damageSource;
	public AudioClip[] ricochetSounds;
	public AudioClip explosionSound;
    [SyncVar]
    Vector3 pos;
    [SyncVar]
    Vector3 forwards;
    [SyncVar]
    Vector3 upwards;
    [SerializeField]
    float minSecsBetweenDeviations = 3;
    float deviationTimer = 3;
    Vector3 groundedPos;
    public ParticleSystem sparks;
	public ParticleSystem damageSparks;
    [SerializeField]
    int deviationsAllowed = 2;
	public int fundsWorth;
    [SerializeField]
    LayerMask mapMask;
    bool onPath = false;
	public bool isDead = false;
    Rigidbody rb;
	public enum Target { PlayerBase, Player, Turret, Node }
	private Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}
    Vector3 oldClosestNode;
    private Target oldTarget;
    private float distanceToTarget;
    private Vector3 oldTargetPos;
    Vector3 currentNodePos;
    float pathLastUpdated;
    int prevIndex = 0;
    public virtual void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("EndZone").transform;
		//UpdatePath ();
        rb = GetComponent<Rigidbody>();
        //  if(GameObject.FindGameObjectWithTag("InWall"))
        //Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
        rb.freezeRotation = true;
        deviationTimer = minSecsBetweenDeviations;
        pos = transform.position;
        hummingSource.clip = hummingSound;
		hummingSource.Play ();
	}
    //bool CheckLineOfSight() {
    //    RaycastHit hit;
    //    List<RaycastHit> hits = new List<RaycastHit>();
    //    foreach (Transform edge in edges) {
    //        if (Physics.Raycast(edge.position, targetPos, out hit, distanceToTarget)) {
    //            if(hit.transform.tag == "Map") {
    //                return false;
    //            }
    //            hits.Add(hit);
    //        }
    //    }
    //    foreach(RaycastHit h in hits) {
    //        if (h.transform.root.gameObject.GetComponent<Player>()) {
    //            target = Target.Player;
    //            return true;
    //        } else if (h.transform.root.gameObject.GetComponent<Base>()) {
    //            target = Target.PlayerBase;
    //            return true;
    //        }
    //    }
    //    return false;
    //}
    //bool CheckLineOfSight() {
    //    if (!isServer) return false;
    //    RaycastHit hit;
    //    List<RaycastHit> hits = new List<RaycastHit>();
    //    foreach (Player player in GameManager.players.Values) { 
    //        foreach (Transform edge in edges) {
    //            if (Physics.Raycast(GroundY(edge.position), GroundY(player.transform.position), out hit)) {
    //                if (hit.transform.tag == "Map") {
    //                    print(transform.name + hit.transform.name+ "NUU2 " );
    //                    return false;
    //                }
    //                hits.Add(hit);
    //            }
    //        }
    //        foreach (RaycastHit h in hits) {
    //            print(h.transform.root.name);
    //            if (h.transform.root.gameObject.GetComponent<Player>()) {
    //                print(transform.name + "NUU3 ");
    //               // targetPos = player.transform.position;
    //                target = Target.Player;
    //                return true;
    //            }
    //        }
    //        print("NUU");
    //    }

    //    return false;
    //}
   
    Vector3? LookForPlayers() {
        if (!isServer) return null;

        Vector3? closestPlayer = null;
        float leastDistance = 0;
        foreach (Player player in GameManager.players.Values) {
            bool broke = false;
            foreach (Transform edge in edges) {
                Vector3 edgePos = GroundY(edge.position);
                Vector3 dir = GroundY(player.transform.position) - edgePos;
                dir.Normalize();
                RaycastHit hit;
                if (Physics.Raycast(edgePos, dir, out hit, viewDistance, mapMask)) {
                    if (hit.transform.tag == "Map") {
                        closestPlayer = null;
                        broke = true;
                        break;
                    } else {
                        Vector3 groundedHitPos = GroundY(hit.transform.position);
                        float distance = Vector3.Distance(groundedHitPos, groundedPos);
                        if (hit.transform.root.gameObject.GetComponent<Player>()) {
                            if (closestPlayer != null) {
                                if (distance < leastDistance) {
                                    closestPlayer = groundedHitPos;
                                    leastDistance = distance;
                                }
                            } else {
                                closestPlayer = groundedHitPos;
                                leastDistance = distance;
                            }
                        }
                    }
                }
            }
            if (broke) break;
        }
        return closestPlayer;
    }
    protected virtual void Update() {
        if (!isServer) {
            pathLastUpdated += Time.deltaTime;
            if (currentHealth <= maxHealth / 2) {
                if (hummingSource.clip.GetInstanceID() != hummingDamageSound.GetInstanceID()) {
                    hummingSource.Stop();
                    hummingSource.clip = hummingDamageSound;
                    hummingSource.Play();
                }

                if (!damageSparks.isPlaying) {
                    damageSparks.Play();
                }
            }
            return;
        }
        if (onPath && !canDeviate && deviationTimer < minSecsBetweenDeviations) {
            deviationTimer += Time.deltaTime;
        }
    }

  
    Vector3 GroundY(Vector3 v) {
        return new Vector3(v.x, 0.1f, v.z);
    }
    protected virtual void FixedUpdate() {
        if (!isServer) {
            transform.position = Vector3.Lerp(transform.position, pos, moveSpeed * 1.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(forwards, upwards), rotSpeed * 1.1f);
            return;
        }        
        //TODO make it so if enemy follows player out of sight of the path the enemy immediately breaks off back to the path
        
        pos = transform.position;
        groundedPos = GroundY(transform.position);
        Vector3? playerPos = LookForPlayers();
        if (!onPath) {
            if (target == Target.Player && playerPos != null) {
                targetPos = (Vector3)playerPos;
            }else if (target != Target.Node && playerPos == null) {
                target = Target.Node;
                Vector3? newClosestNode = EnemyManager.GetClosestPathNodePos(groundedPos, edges, mapMask);
                if (newClosestNode != null) {
                    targetPos = (Vector3)newClosestNode;
                    oldClosestNode = targetPos;
                } else {
                    targetPos = oldClosestNode;
                }
                currentPathIndex = EnemyManager.GetPathIndexFromPos(targetPos, 0.1f);
            } else if (target == Target.Node && distanceToTarget < offsetToUpdatePath) {
                onPath = true;
            }
        }
        if(onPath) {
            if (!canDeviate && deviationsAllowed > 0 && deviationTimer >= minSecsBetweenDeviations) {
                canDeviate = true;
                deviationsAllowed--;
            }
            if (canDeviate && playerPos != null) {
                target = Target.Player;
                onPath = false;
                targetPos = (Vector3)playerPos;
                canDeviate = false;
                deviationTimer = 0;
            } else if (distanceToTarget < offsetToUpdatePath) {
                target = Target.PlayerBase;
                currentPathIndex++;
                targetPos = EnemyManager.path[currentPathIndex];
            }
        }

        transform.rotation = Rotate(targetPos);
        transform.position = Move(targetPos);
        distanceToTarget = GetDistanceToTarget(targetPos);
    }

    float GetDistanceToTarget(Vector3 tPos) {
        return Vector3.Distance(tPos, groundedPos);
    }

    Vector3 Move(Vector3 tPos) {
        return pos + transform.rotation * Vector3.forward * moveSpeed;
    }

    Vector3 RemoveY(Vector3 v3) {
        return new Vector3(v3.x, 0, v3.z);
    }

    Quaternion Rotate(Vector3 tPos) {
        Vector3 targetDirection = tPos - groundedPos;
        targetDirection.Normalize();
        targetDirection = RemoveY(targetDirection);
        return transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection), rotSpeed);
    }
    
    void OnTriggerEnter(Collider collider) {
        if (collider.transform.name == "EndZone") {
            Die();
            return;
        }
    }
    void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts) {
            if (contact.otherCollider.transform.name == "EndZone") {
                Die();
                return;
            } else if (contact.otherCollider.transform.name.Contains("Player")) {
                Die();
                return;
            }
        }
    }
    protected override void Die() {
        if (isServer) {
            Explode();

            Enemy enemy = default(Enemy);
            string ID = "";
            foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
                if (kp.Value.transform.position == transform.position) {
                    enemy = kp.Value;
                    ID = kp.Key;
                    break;
                }
            }
            if (enemy != default(Enemy)) {

                EnemyManager.enemies.Remove(ID);
                NetworkServer.Destroy(enemy.gameObject);
            }
        }
    }
    protected virtual void Explode() {
        //damage playerbase if in range
        if (Vector3.Distance(playerBase.position, transform.position) < weaponRange) {
            playerBase.GetComponent<Base>().TakeDamage(weaponDamage);
            Debug.Log("Enemy hit base for: " + weaponDamage);
        }
        //damage players in range
        List<Player> playersInRange = GameManager.GetPlayersInRange(transform.position, weaponRange);
        if (playersInRange.Count > 0) {
            foreach (Player player in playersInRange) {
                player.TakeDamage(weaponDamage);

            }
        }
        GameObject exp = (GameObject)GameObject.Instantiate(explosion, transform.position, new Quaternion());
        NetworkServer.Spawn(exp);
        isDead = true;
    }

    public override void TakeDamage(float _dmg) {
     
        sparks.Play();

        //For playing sounds only.
        int i = UnityEngine.Random.Range(0, ricochetSounds.Length);

        AudioClip clip = ricochetSounds[i];
        damageSource.Stop();
        damageSource.clip = clip;
        damageSource.Play();
        base.TakeDamage(_dmg);
    }
   
}
