﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//class adds pathfinding
//initializeSearchNodes creates the node Grid and should be recalled if the map changes
//find path returns the optimal G from vector 1 to vector 2
public class Pathfinder : MonoBehaviour
{
	//holds the nodes to pathfind through
	private SearchNode[,] searchNodes;
	//width and height of grids in the map
	private int levelWidth, levelHeight;
	// Holds search nodes that are avaliable to search.
	private List<SearchNode> openList = new List<SearchNode> ();
	// Holds the nodes that have already been searched.
	private List<SearchNode> closedList = new List<SearchNode> ();
	private float nearWallMoveCost = 0.25f;
    public bool isPathfinding = false;
	public Pathfinder ()
	{
		initializeSearchNodes ();
	}
    public void initializeSearchNodes ()
	{
		levelWidth = Grid.gridSizeX;
		levelHeight = Grid.gridSizeY;
		//creates and initualises the search nodes
		searchNodes = new SearchNode[levelWidth, levelHeight];
		createNodeGrid ();
		//initialise the neighbor links 
		createNeighbourLinks();
		//handle corners
		findAndRemoveCorners();
		//discourage wall hugging
		increaseWallMoveCost();

	}
	private void increaseWallMoveCost()
	{
		foreach (SearchNode node in searchNodes) {
			if (!node.walkable) {
				changeNeighboursMoveCost (node, nearWallMoveCost);
			}
		}
	}
	private void changeNeighboursMoveCost(SearchNode node, float amount){
		foreach (SearchNode neighbour in node.neighbors) {
			if (neighbour != null && neighbour.walkable) {
				neighbour.moveCost += amount;
			}
		}
	}
	private void findAndRemoveCorners(){
		foreach(SearchNode node in searchNodes){ 
				if (isCorner (node)) {
					removeCornerNeighbours (node);
				}					
			}		
	}
	private bool isCorner(SearchNode n){
		if (n.walkable) {
			return false;
		}
		//is up neighbour walkable
		bool nUp = n.neighbors [0] != null && n.neighbors [0].walkable; 
		//is down neighbour walkable
		bool nDn = n.neighbors [1] != null && n.neighbors [1].walkable;
		//is left neighbour walkable
		bool nLf = n.neighbors [2] != null && n.neighbors [2].walkable;		
		//is right neighbour walkable
		bool nRt = n.neighbors [3] != null && n.neighbors [3].walkable;
		//if a vertical direction and horrizontal direction walkable then its a corner
		return (nUp||nDn) && (nLf||nRt);
	}
	private void removeCornerNeighbours(SearchNode n){
		if (n.neighbors[0] != null && n.neighbors [0].walkable) {
			if (n.neighbors [2] != null && n.neighbors [2].walkable) {
				n.neighbors [0].neighbors [6] = null;
				n.neighbors [2].neighbors [5] = null;
			}
			if (n.neighbors [3] != null && n.neighbors [3].walkable) {
				n.neighbors [0].neighbors [7] = null;
				n.neighbors [3].neighbors [4] = null;
			}
		}
		if (n.neighbors[1] != null && n.neighbors [1].walkable) {
			if (n.neighbors [2] != null && n.neighbors [2].walkable) {
				n.neighbors [1].neighbors [4] = null;
				n.neighbors [2].neighbors [7] = null;
			}
			if (n.neighbors [3] != null && n.neighbors [3].walkable) {
				n.neighbors [1].neighbors [5] = null;
				n.neighbors [3].neighbors [6] = null;
			}
		}
	}
	private void restoreCornerNeighbours(SearchNode n){
		if (n.neighbors[0] != null) {
			if (n.neighbors [2] != null) {
				n.neighbors [0].neighbors [6] = searchNodes [(int)n.position.x - 1, (int)n.position.y + 1];
				n.neighbors [2].neighbors [5] = searchNodes[(int)n.position.x + 1, (int)n.position.y - 1];
			}
			if (n.neighbors [3] != null) {
				n.neighbors [0].neighbors [7] = searchNodes[(int)n.position.x + 1, (int)n.position.y + 1];
				n.neighbors [3].neighbors [4] = searchNodes[(int)n.position.x - 1, (int)n.position.y - 1];
			}
		}
		if (n.neighbors[1] != null) {
			if (n.neighbors [2] != null) {
				n.neighbors [1].neighbors [4] = searchNodes[(int)n.position.x - 1, (int)n.position.y - 1];
				n.neighbors [2].neighbors [7] = searchNodes[(int)n.position.x + 1, (int)n.position.y + 1];
			}
			if (n.neighbors [3] != null) {
				n.neighbors [1].neighbors [5] = searchNodes[(int)n.position.x + 1, (int)n.position.y - 1];
				n.neighbors [3].neighbors [6] = searchNodes [(int)n.position.x - 1, (int)n.position.y + 1];
			}
		}
	}
	private void createNodeGrid()
	{
		for (int x = 0; x < levelWidth; x++) {
			for (int y = 0; y < levelHeight; y++) {
				SearchNode node = new SearchNode ();
				node.position = new Vector2 (x, y);
				node.walkable = Grid.GetWalkable (x, y);
                node.largeWalkable = Grid.grid[x, y].largeWalkable;
				node.moveCost = 1;//Grid.getMoveCost
				//commenting this out stops un walkable nodes being null
				//if (node.walkable == true) {
				node.neighbors = new SearchNode[8];
				searchNodes [x, y] = node;
				//}
			}
		}
		return;
	}
	private void createNeighbourLinks()
	{
		for (int x = 0; x < levelWidth; x++) {
			for (int y = 0; y < levelHeight; y++) {
				SearchNode node = searchNodes [x, y];
				/*commenting this out means non walkable nodes can be pathfinded out of
				 * if (node == null || node.walkable == false) {
					continue;
				}*/
				Vector2[] neighbors = new Vector2[] {
					new Vector2 (x, y - 1), // 0 The node above the current node
					new Vector2 (x, y + 1), // 1 The node below the current node.
					new Vector2 (x - 1, y), // 2 The node left of the current node.
					new Vector2 (x + 1, y), // 3 The node right of the current node
					new Vector2 (x - 1, y - 1), // 4 The node above and left of the current node
					new Vector2 (x + 1, y - 1), // 5 The node above and right of the current node
					new Vector2 (x - 1, y + 1), // 6 The node below and left of the current node
					new Vector2 (x + 1, y + 1), // 7 The node below and right of the current node

				};
				for (int i = 0; i < neighbors.Length; i++) {
					Vector2 position = neighbors [i];
					if (position.x < 0 || position.x > levelWidth - 1 || position.y < 0 || position.y > levelHeight - 1) {
						continue;
					}
					SearchNode neighbor = searchNodes [(int)position.x, (int)position.y];
					if (neighbor == null || neighbor.walkable == false) {
						continue;
					}
					node.neighbors [i] = neighbor;
				}
			}
		}
		return;
	}
	public void SetUnwalkable(int x, int y){
		SearchNode node = searchNodes[x,y];
		if (isCorner (node)) {
			removeCornerNeighbours (node);
		}
		changeNeighboursMoveCost (node, nearWallMoveCost);
		node.walkable = false;
	}
	public void SetWalkable(int x, int y){
		SearchNode node = searchNodes[x,y];
		restoreCornerNeighbours (node);
		changeNeighboursMoveCost (node, -nearWallMoveCost);
		node.walkable = true;
	}
	public bool checkWalkable(int x, int y){
		return searchNodes [x, y].walkable;
	}
	//return the manhattan distance distance between two vectors
	private float heuristic (Vector2 v1, Vector2 v2)
	{
		return Mathf.Abs (v1.x - v2.x) +
			Mathf.Abs (v1.y - v2.y);
	}

	//resets the search nodes
	private void resetSearchNodes ()
	{
		openList.Clear ();
		closedList.Clear ();
		foreach(SearchNode node in searchNodes){
				if (node == null) {
					continue;
				}
				node.inOpenList = false;
				node.inClosedList = false;
				node.distanceTraveled = float.MaxValue;
				node.distanceToGoal = float.MaxValue;
			
		}
	}
    void ResetDoublePass(List<SearchNode> cl) {
        foreach(SearchNode sn in cl) {
            sn.justChecked = false;
        }
    }
    public bool IsBlockingPath(Vector2 _pos, Vector2 _targetPos) {
        List<SearchNode> firstPass;
        if (findPath(_pos, _targetPos, true) == new List<Vector2>()) {
            firstPass = closedList;
            ResetDoublePass(firstPass);
            return true;
        }
        firstPass = closedList;
        if (findPath(_pos, _targetPos, false) == new List<Vector2>()) {
            return true;
        }
        ResetDoublePass(firstPass);
        return false;
    }
    // Returns the node with the smallest distance to goal.
    private SearchNode findBestNode ()
	{
		SearchNode currentTile = openList [0];
		float smallestDistanceToGoal = float.MaxValue;
		// Find the closest node to the goal.
		for (int i = 0; i < openList.Count; i++) {
			if (openList [i].distanceToGoal < smallestDistanceToGoal) {
				currentTile = openList [i];
				smallestDistanceToGoal = currentTile.distanceToGoal;
			}
		}
		return currentTile;
	}
	// Use the parent field of the search nodes to trace
	// a path from the end node to the start node.
	private List<Vector2> findFinalPath (SearchNode startNode, SearchNode endNode)
	{
		closedList.Add (endNode);
		SearchNode parentTile = endNode.parent;
		// Trace back through the nodes using the parent fields
		// to find the best path.
		while (parentTile != startNode) {
			closedList.Add (parentTile);
			parentTile = parentTile.parent;
		}
		List<Vector2> finalPath = new List<Vector2> ();
		// Reverse the path
		for (int i = closedList.Count - 1; i >= 0; i--) {
			finalPath.Add (new Vector2 (closedList [i].position.x, closedList [i].position.y));
		}
		return finalPath;
	}
    public List<Vector3> FindVector3Path(Vector3 startPoint, Vector3 endPoint, bool largePath) {
        List<Vector3> vector3Path = new List<Vector3>();
        Vector2 sp = Grid.GetVector2(startPoint);
        Vector2 ep = Grid.GetVector2(endPoint);
        List<Vector2> path = findPath(sp, ep, largePath);
        if(path.Count == 0) {
            return new List<Vector3>();
        }
        foreach(Vector2 node in path) {
            vector3Path.Add(Grid.GetWorldPos(node));
        }
        return vector3Path;
    }
	// Finds the optimal path from one point to another.
	public List<Vector2> findPath (Vector2 startPoint, Vector2 endPoint, bool largePath)
	{
        isPathfinding = true;
		// Only try to find a path if the start and end points are different.
		if (startPoint == endPoint) {
            isPathfinding = false;
            return new List<Vector2> ();
		}

		/////////////////////////////////////////////////////////////////////
		// Step 1 : Clear the Open and Closed Lists and reset each node’s F 
		//          and G values in case they are still set from the last 
		//          time we tried to find a path. 
		/////////////////////////////////////////////////////////////////////
		resetSearchNodes ();

		// Store references to the start and end nodes for convenience.
		SearchNode startNode = searchNodes [(int)startPoint.x, (int)startPoint.y];
		SearchNode endNode = searchNodes [(int)endPoint.x, (int)endPoint.y];
        if (endNode.walkable == false) {
            isPathfinding = false;
            return new List<Vector2>();
        }
		/////////////////////////////////////////////////////////////////////
		// Step 2 : Set the start node’s G value to 0 and its F value to the 
		//          estimated distance between the start node and goal node 
		//          (this is where our H function comes in) and add it to the 
		//          Open List. 
		/////////////////////////////////////////////////////////////////////
		startNode.inOpenList = true;

		startNode.distanceToGoal = heuristic (startPoint, endPoint);
		startNode.distanceTraveled = 0;

		openList.Add (startNode);

		/////////////////////////////////////////////////////////////////////
		// Setp 3 : While there are still nodes to look at in the Open list : 
		/////////////////////////////////////////////////////////////////////
		while (openList.Count > 0) {
			/////////////////////////////////////////////////////////////////
			// a) : Loop through the Open List and find the node that 
			//      has the smallest F value.
			/////////////////////////////////////////////////////////////////
			SearchNode currentNode = findBestNode ();

			/////////////////////////////////////////////////////////////////
			// b) : If the Open List empty or no node can be found, 
			//      no path can be found so the algorithm terminates.
			/////////////////////////////////////////////////////////////////
			if (currentNode == null) {
				break;
			}

			/////////////////////////////////////////////////////////////////
			// c) : If the Active Node is the goal node, we will 
			//      find and return the final path.
			/////////////////////////////////////////////////////////////////
			if (currentNode == endNode) {
                isPathfinding = false;
                // Trace our path back to the start.
                return findFinalPath (startNode, endNode);
			}

			/////////////////////////////////////////////////////////////////
			// d) : Else, for each of the Active Node’s neighbours :
			/////////////////////////////////////////////////////////////////
			for (int i = 0; i < currentNode.neighbors.Length; i++) {
				SearchNode neighbor = currentNode.neighbors [i];

                //////////////////////////////////////////////////
                // i) : Make sure that the neighbouring node can 
                //      be walked across. 
                //////////////////////////////////////////////////
                if (neighbor == null || !neighbor.walkable || (largePath && !neighbor.largeWalkable)) {
					continue;
				}

				//////////////////////////////////////////////////
				// ii) Calculate a new G value for the neighbouring node.
				//////////////////////////////////////////////////
				float distanceTraveled = currentNode.distanceTraveled + currentNode.moveCost;

				// An estimate of the distance from this node to the end node.
				float heuristicVal = heuristic (neighbor.position, endPoint);

				//////////////////////////////////////////////////
				// iii) If the neighbouring node is not in either the Open 
				//      List or the Closed List : 
				//////////////////////////////////////////////////
				if (neighbor.inOpenList == false && neighbor.inClosedList == false) {
					// (1) Set the neighbouring node’s G value to the G value 
					//     we just calculated.
					neighbor.distanceTraveled = distanceTraveled;
					// (2) Set the neighbouring node’s F value to the new G value + 
					//     the estimated distance between the neighbouring node and
					//     goal node.
					neighbor.distanceToGoal = distanceTraveled + heuristicVal;
					// (3) Set the neighbouring node’s Parent property to point at the Active 
					//     Node.
					neighbor.parent = currentNode;
					// (4) Add the neighbouring node to the Open List.
					neighbor.inOpenList = true;
					openList.Add (neighbor);
				}
				//////////////////////////////////////////////////
				// iv) Else if the neighbouring node is in either the Open 
				//     List or the Closed List :
				//////////////////////////////////////////////////
				else if (neighbor.inOpenList || neighbor.inClosedList) {
					// (1) If our new G value is less than the neighbouring 
					//     node’s G value, we basically do exactly the same 
					//     steps as if the nodes are not in the Open and 
					//     Closed Lists except we do not need to add this node 
					//     the Open List again.
					if (neighbor.distanceTraveled > distanceTraveled) {
						neighbor.distanceTraveled = distanceTraveled;
						neighbor.distanceToGoal = distanceTraveled + heuristicVal;

						neighbor.parent = currentNode;
					}
				}
			}

			/////////////////////////////////////////////////////////////////
			// e) Remove the Active Node from the Open List and add it to the 
			//    Closed List
			/////////////////////////////////////////////////////////////////
			openList.Remove (currentNode);
			currentNode.inClosedList = true;
		}

        isPathfinding = false;
        // No path could be found.
        return new List<Vector2> ();
	}

	//nodes that store pathfinding information
	private class SearchNode
	{
		public Vector2 position;
		public bool walkable, largeWalkable, inOpenList, inClosedList, justChecked;
		public SearchNode[] neighbors;
		public SearchNode parent;
		public float distanceToGoal, distanceTraveled, moveCost;
	}
}	