﻿using UnityEngine;
using System.Collections;

public struct GridNode {
	private bool _walkable;
	public bool walkable {
		get { return _walkable; }
		set { _walkable = value; }
	}
    private bool _largeWalkable;
    public bool largeWalkable {
        get { return _largeWalkable; }
        set { _largeWalkable = value; }
    }
    private Vector3 _worldPos;
	public Vector3 worldPos {
		get { return _worldPos; }
		set { _worldPos = value; }
	}

	public GridNode(bool walkable, bool largeWalkable, Vector3 worldPos){
		this._walkable = walkable;
        this._largeWalkable = largeWalkable;
        this._worldPos = worldPos;
	}
    public GridNode(bool walkable, Vector3 worldPos) {
        this._walkable = walkable;
        this._largeWalkable = true;
        this._worldPos = worldPos;
    }

    public bool GetWalkable(){
		return _walkable;
	}
	public void SetWalkable(bool w){
		walkable = w;
	}
	public static bool operator ==(GridNode gn1, GridNode gn2){
		return gn1.Equals (gn2);
	}
	public static bool operator !=(GridNode gn1, GridNode gn2){
		return !gn1.Equals (gn2);
	}
}
