﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class TutorialScript : NetworkBehaviour {
	//TODO: FIX SHOOTING TEST. IMPLEMENT ENEMY AND WAVE TEST

	public GameObject tutorialUI; //This should be a prefab, that can be instantiated.
	public bool isShowingUI = false;
	private GameObject currentlyShowingTutorialUI;
	private TutorialStage currentStage;

	int indexOfStage = 0;

	List<TutorialStage> stages = new List<TutorialStage> ();

	//Tutorial flags
	private bool wPressed = false;
	private bool sPressed = false;
	private bool aPressed = false;
	private bool dPressed = false;
	private bool spacePressed = false;

	private bool towerPlaced = false;

	private bool storeOpened = false;
	private bool weaponBought = false;
	private bool weaponShot = false;

	private bool enemyKilled = false;
	private bool enemyWasAlive = false;

	private Enemy enemy;

	// Use this for initialization
	void Start () {
		SetUpTutorialStages ();
		indexOfStage = 12;

		currentStage = stages.ToArray () [indexOfStage];
	}

	private void SetUpTutorialStages(){
		TutorialStage preamble = new TutorialStage (TextDisplayComplete, "Welcome to the tutorial for The Crystal Guard!", "Welcome!");
		TutorialStage stage1 = new TutorialStage (Stage1Complete, "Move using the WASD keys. Jump with Space.", "Move and jump.");
		TutorialStage congrats = new TutorialStage (TextDisplayComplete, "Well done.", "You did it!");
		TutorialStage stage2 = new TutorialStage (Stage2Complete, "Place a tower by looking down at the ground, and clicking the Left Mouse Button.", "Place a tower.");
		TutorialStage congrats2 = new TutorialStage (TextDisplayComplete, "Well done.", "You did it!");
		TutorialStage stage3 = new TutorialStage (Stage3Complete, "To buy a weapon, you need to open the Store. Open the Store by pressing the TAB button." , "Open the Store.");
		TutorialStage congrats3 = new TutorialStage (TextDisplayComplete, "Well done.", "You did it!");
		TutorialStage stage4 = new TutorialStage (Stage4Complete, "Buy a weapon, and then exit the store.", "Buy a weapon.");
		TutorialStage congrats4 = new TutorialStage (TextDisplayComplete, "Well done.", "You did it!");
		TutorialStage stage5 = new TutorialStage (Stage5Complete, "Take your weapon out for a test drive. Fire your weapon by pressing the Left Mouse Button." , "Fire your weapon.");
		TutorialStage congrats5 = new TutorialStage (TextDisplayComplete, "Well done.", "You did it!");
		TutorialStage stage6 = new TutorialStage (Stage6Complete, "Now it's time for your first enemy. Destroy the enemy drone!" , "Destroy the drone.");
		TutorialStage finish = new TutorialStage (TextDisplayComplete, "Congratulations! You have finished the tutorial. Return to the Main Menu." , "You did it!");


		stages.Add (preamble);   //0
		stages.Add (stage1);     //1
		stages.Add (congrats);   //2
		stages.Add (stage2);     //3
		stages.Add (congrats2);  //4
		stages.Add (stage3);     //5
		stages.Add (congrats3);  //6
		stages.Add (stage4);     //7
		stages.Add (congrats4);  //8
		stages.Add (stage5);     //9
		stages.Add (congrats5);  //10
		stages.Add (stage6);     //11
		stages.Add (congrats);   //12
		stages.Add(finish);
	}
	
	// Update is called once per frame
	void Update () {
        if (!isServer) return;
		if (isShowingUI) {

			if (currentlyShowingTutorialUI.GetComponent<TutorialUIScript> ().isButtonClicked) {
				CloseUIWindow ();
				isShowingUI = false;

				if (indexOfStage == stages.Count - 1) {
					PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = false;
					PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = false;
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = true;
                    NetworkServer.Shutdown();
                    NetworkManager.Shutdown();
					SceneManager.LoadScene ("MainMenu");
				}
			}

			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = true;
			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = true;

		}else {

			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = false;
			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = false;

			if (!currentStage.hasShownPrompt) {
				SpawnUIWindow (currentStage.GetPrompts (), "Close");
				currentStage.hasShownPrompt = true;
			}
			else {
				UpdateFlags ();
				if (currentStage.IsCompleted ()) {
					currentStage.hasBeenCompleted = true;
					indexOfStage =  indexOfStage + 1;
					currentStage = stages.ToArray () [indexOfStage];

					if (indexOfStage == 11) {
						EnemyManager.singleton.SpawnEnemy(Enemy.FLYING, new Vector3(3, .5f, 0), 5);
						enemy = EnemyManager.GetClosestEnemy (PlayerHelper.GetPlayer ().transform.position);
						enemyWasAlive = true;
					}

					//SpawnUIWindow ("Well done.", "Next");
				}
			}

		}

	}

	public void CloseUIWindow(){
		currentlyShowingTutorialUI.SetActive (false);
		Destroy(currentlyShowingTutorialUI);	
	}

	private void SpawnUIWindow(string text){
		SpawnUIWindow (text, "Next");
	}

	private void SpawnUIWindow(string text, string buttonText){
		currentlyShowingTutorialUI  = (GameObject)(Instantiate (tutorialUI, new Vector3(), new Quaternion(), this.transform));
		currentlyShowingTutorialUI.GetComponent<TutorialUIScript> ().SetUIText (text, buttonText);
		isShowingUI = true;
		GameObject.FindGameObjectWithTag ("PlayerUI").GetComponent<PlayerUI> ().txtPhase.text = currentStage.GetPhasePrompt ();

	}



	private void UpdateFlags(){

		//Movement test
		if (Input.GetKeyDown (KeyCode.W)) {
			wPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			sPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			aPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			dPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			spacePressed = true;
		}

	
		//Tower placing test
		if (Stage1Complete ()) {
			if (TowerManager.GetClosestTower (PlayerHelper.GetPlayer ().transform.position) != null) {
				towerPlaced = true;
			}
		}

		//Entering shop test
		if (Stage2Complete ()) {
			if (GameObject.FindGameObjectWithTag ("PlayerUI").GetComponent<PlayerUI> ().showShop) {
				storeOpened = true;
			}
		}

		//Buying weapon test
		if (Stage3Complete ()) {
			WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
			if (weaponManager.GetCurrentWeapon ().gunID != 0) {
				weaponBought = true;
			}
		}

		//Shooting weapon test
		if (Stage4Complete ()) {
			WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
			BaseWeapon currentWeapon = weaponManager.GetCurrentWeapon ();
			if (currentWeapon.gunID != 0 && Input.GetMouseButton(0)) {
				weaponShot = true;
			}
		}

		//Killing enemy test
		if (Stage5Complete ()) {
			if (enemy != null) {
				if (enemy.isDead) {
					enemyKilled = true;
				}
			} else {
				if (enemyWasAlive) {
					enemyKilled = true;
				}
			}
		}
	}


	public bool TextDisplayComplete(){
		return true;
	}

	public bool Stage1Complete(){
		return wPressed && sPressed && aPressed && dPressed && spacePressed;
	}

	public bool Stage2Complete(){
		return towerPlaced;
	}

	public bool Stage3Complete(){
		return storeOpened;
	}

	public bool Stage4Complete(){
		return weaponBought && GameObject.FindGameObjectWithTag("PlayerUI").GetComponent<PlayerUI>().showShop == false;
	}

	public bool Stage5Complete(){
		WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
		BaseWeapon currentWeapon = weaponManager.GetCurrentWeapon ();
		return weaponShot && !currentWeapon.gunAnim.isPlaying;
	}

	public bool Stage6Complete(){
		return enemyKilled;
	}


}
